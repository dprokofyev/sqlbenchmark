CREATE TABLE `counterparties` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `full_name` varchar(255)
);

CREATE TABLE `api_keys` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `counterparty_id` int,
  `apikey` varchar(255),
  `valid` boolean
);

CREATE TABLE `products` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(255),
  `display_name` varchar(255),
  `internal_name` varchar(255),
  `product_type` varchar(255)
);

CREATE TABLE `quotes` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `created_at` datetime DEFAULT (now()),
  `expire_at` datetime,
  `accepted_at` datetime,
  `product_id` int,
  `counterparty_id` int,
  `price` double,
  `quantity` double,
  `way` ENUM ('buy', 'sell')
);

CREATE TABLE `trades` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `quote_id` int UNIQUE,
  `accepted_at` datetime,
  `product_id` int,
  `counterparty_id` int,
  `price` double,
  `quantity` double,
  `way` ENUM ('buy', 'sell')
);

ALTER TABLE `counterparties` ADD FOREIGN KEY (`id`) REFERENCES `api_keys` (`counterparty_id`);

ALTER TABLE `products` ADD FOREIGN KEY (`id`) REFERENCES `quotes` (`product_id`);

ALTER TABLE `counterparties` ADD FOREIGN KEY (`id`) REFERENCES `quotes` (`counterparty_id`);

ALTER TABLE `trades` ADD FOREIGN KEY (`quote_id`) REFERENCES `quotes` (`id`);

ALTER TABLE `products` ADD FOREIGN KEY (`id`) REFERENCES `trades` (`product_id`);

ALTER TABLE `counterparties` ADD FOREIGN KEY (`id`) REFERENCES `trades` (`counterparty_id`);