CI_REGISTRY=registry.gitlab.com/dprokofyev
CI_REGISTRY_IMAGE = ${CI_REGISTRY}/sqlbenchmark
CI_REGISTRY_USER = dprokofyev
CI_REGISTRY_PASSWORD=yoXx21u2ox-N5dY3rJ7U
CI_COMMIT_SHORT_SHA= $$(git rev-parse --short=7 HEAD)

build:
	gitlab-runner exec docker --docker-volumes /var/run/docker.sock:/var/run/docker.sock --docker-privileged --env CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} --env CI_REGISTRY_USER=${CI_REGISTRY_USER} --env CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD} --env CI_REGISTRY=${CI_REGISTRY} --env CI_COMMIT_SHORT_SHA=$(git rev-parse --short=7 HEAD) build
test-integration:
	gitlab-runner exec docker --docker-volumes /var/run/docker.sock:/var/run/docker.sock --docker-privileged --env CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE} --env CI_REGISTRY_USER=${CI_REGISTRY_USER} --env CI_REGISTRY_PASSWORD=${CI_REGISTRY_PASSWORD} --env CI_REGISTRY=${CI_REGISTRY} --env CI_COMMIT_SHORT_SHA=$(git rev-parse --short=7 HEAD) test_integration

