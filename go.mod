module SqlBenchmark

go 1.16

require (
	github.com/jackc/pgproto3/v2 v2.1.0 // indirect
	github.com/lib/pq v1.10.2
	github.com/shopspring/decimal v1.2.0
	golang.org/x/crypto v0.0.0-20210616213533-5ff15b29337e // indirect
	golang.org/x/text v0.3.6 // indirect
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.11
)
