-- name: GetQuote :one
SELECT * FROM quotes
WHERE id = $1 LIMIT 1;

-- name: CreateQuote :exec
INSERT INTO quotes (
  created_at,
  expire_at,
  product_id,
  counterparty_id,
  price,
  quantity,
  way
) VALUES (
  $1, $2, $3, $4, $5, $6, $7
);

-- name: GetLastQuote :exec
INSERT INTO quotes (
  created_at,
  expire_at,
  product_id,
  counterparty_id,
  price,
  quantity,
  way
) VALUES (
  $1, $2, $3, $4, $5, $6, $7
);


-- name: AcceptQuote :exec
UPDATE quotes  SET  accepted_at = $2
WHERE id = $1;