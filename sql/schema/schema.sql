CREATE TYPE "trade_way" AS ENUM (
  'buy',
  'sell'
);

CREATE TYPE "product_type" AS ENUM (
  'spot',
  'futures',
  'options'
);

CREATE TABLE "counterparties" (
  "id" SERIAL PRIMARY KEY,
  "full_name" varchar
);

CREATE TABLE "api_keys" (
  "id" SERIAL PRIMARY KEY,
  "counterparty_id" int,
  "apikey" varchar,
  "valid" boolean
);

CREATE TABLE "products" (
  "id" SERIAL PRIMARY KEY,
  "name" varchar,
  "display_name" varchar,
  "internal_name" varchar,
  "product_type" varchar
);

CREATE TABLE "quotes" (
  "id" SERIAL PRIMARY KEY,
  "created_at" timestamp DEFAULT (now()),
  "expire_at" timestamp,
  "accepted_at" timestamp,
  "product_id" int,
  "counterparty_id" int,
  "price" numeric,
  "quantity" numeric,
  "way" trade_way
);

CREATE TABLE "trades" (
  "id" SERIAL PRIMARY KEY,
  "quote_id" int UNIQUE,
  "accepted_at" timestamp,
  "product_id" int,
  "counterparty_id" int,
  "price" numeric,
  "quantity" numeric,
  "way" trade_way
);


alter table "api_keys" add foreign key ("counterparty_id") references "counterparties" ("id");

alter table "quotes" add foreign key ("product_id") REFERENCES "products" ("id");

ALTER TABLE "trades" ADD FOREIGN KEY ("quote_id") REFERENCES "quotes" ("id");

ALTER TABLE "quotes" ADD FOREIGN KEY ("counterparty_id") REFERENCES "counterparties" ("id");

ALTER TABLE "trades" ADD FOREIGN KEY ("counterparty_id") REFERENCES "counterparties" ("id");

ALTER TABLE "trades" ADD FOREIGN KEY ("product_id") REFERENCES "products" ("id");

INSERT INTO public.products (id, name, display_name, internal_name, product_type) VALUES (1, 'btcusd', 'btcusd', 'btcusd', 'spot');
INSERT INTO public.products (id, name, display_name, internal_name, product_type) VALUES (2, 'ethusd', 'ethusd', 'ethusd', 'spot');

INSERT INTO public.counterparties (id, full_name) VALUES (1, 'KT');
INSERT INTO public.counterparties (id, full_name) VALUES (2, 'LCG');
