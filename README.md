#Performance test Sqlc Vs GORM

``go test -bench ./...``

```bash
goos: linux
goarch: amd64
pkg: SqlBenchmark
cpu: Intel(R) Core(TM) i7-10510U CPU @ 1.80GHz
BenchmarkInsert/gorm_insert-8                 31          37034478 ns/op
BenchmarkInsert/sqlc_insert-8                 15          73308285 ns/op
BenchmarkSelect/gorm_select-8                 32          36524727 ns/op
BenchmarkSelect/sqlc_select-8                 15          73009149 ns/op
BenchmarkUpdate/gorm_update-8                 31          36805217 ns/op
BenchmarkUpdate/sqlc_update-8                 15          73282035 ns/op
PASS
ok      SqlBenchmark    9.950s
```