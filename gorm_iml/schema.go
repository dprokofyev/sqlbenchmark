package gorm_iml

import (
	"github.com/shopspring/decimal"
	"gorm.io/gorm"
	"time"
)

type Way string

const (
	QuotesWayBuy  Way = "buy"
	QuotesWaySell Way = "sell"
)

type Product struct {
	gorm.Model
	ID           int32
	Name         string
	DisplayName  string
	InternalName string
	ProductType  string
}

type Quote struct {
	ID             int32
	CreatedAt      time.Time
	ExpireAt       time.Time
	AcceptedAt     time.Time
	ProductID      int32
	CounterpartyID int32
	Price          decimal.Decimal `sql:"type:decimal(20,8);"`
	Quantity       decimal.Decimal `sql:"type:decimal(20,8);"`
	Way            Way
}

type Trade struct {
	ID             int32
	QuoteID        int32
	AcceptedAt     time.Time
	ProductID      int32
	CounterpartyID int32
	Price          decimal.Decimal `sql:"type:decimal(20,8);"`
	Quantity       decimal.Decimal `sql:"type:decimal(20,8);"`
	Way            Way
}
