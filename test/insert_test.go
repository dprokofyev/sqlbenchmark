package test

import (
	"SqlBenchmark/gorm_iml"
	"SqlBenchmark/sqlc_iml/db"
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/shopspring/decimal"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"math/rand"
	"os"
	"testing"
	"time"
)

func setup_connection() (*gorm.DB, *db.Queries) {
	dsn := os.Getenv("DSN")
	gormDb, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		SkipDefaultTransaction: true,
		PrepareStmt:            true,
	})

	conn, err2 := sql.Open("postgres", dsn)
	conn.SetMaxOpenConns(0)
	conn.SetMaxIdleConns(5)
	conn.SetConnMaxLifetime(time.Hour)
	sqlcDb := &db.Queries{conn}

	if err != nil || err2 != nil {
		fmt.Print(err)
		fmt.Print(err2)
		panic("Cannot connect to DB")
	}

	return gormDb, sqlcDb
}

func BenchmarkInsert(b *testing.B) {

	gormDb, sqlcDb := setup_connection()
	ctx := context.Background()

	b.Run(fmt.Sprintf("gorm insert"), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			q := &gorm_iml.Quote{
				CreatedAt:      time.Now(),
				ExpireAt:       time.Now(),
				ProductID:      int32(rand.Intn(1) + 1),
				CounterpartyID: 1,
				Price:          decimal.NewFromFloat(rand.Float64() * 64000),
				Quantity:       decimal.NewFromFloat(rand.Float64() * 100),
				Way:            gorm_iml.QuotesWaySell,
			}
			err := gormDb.Create(q).Error
			if err != nil {
				b.Fatal(err)
			}
		}
	})

	// Benchmark sqlc
	b.Run(fmt.Sprintf("sqlc insert"), func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			err := sqlcDb.CreateQuote(ctx, db.CreateQuoteParams{
				CreatedAt:      sql.NullTime{Time: time.Now(), Valid: true},
				ExpireAt:       sql.NullTime{Time: time.Now(), Valid: true},
				ProductID:      sql.NullInt32{Int32: int32(rand.Intn(1) + 1), Valid: true},
				CounterpartyID: sql.NullInt32{Int32: 2, Valid: true},
				Price:          sql.NullString{String: decimal.NewFromFloat(rand.Float64() * 64000).String(), Valid: true},
				Quantity:       sql.NullString{String: decimal.NewFromFloat(rand.Float64() * 100).String(), Valid: true},
				Way:            db.TradeWaySell,
			})
			if err != nil {
				b.Fatal("Sql create failed")
			}
		}
	})
}
